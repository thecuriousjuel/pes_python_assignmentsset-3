def add(a, b):
	return a + b


def diff(a, b):
	return a - b


def mul(a, b):
	return a * b


def sqrt(n):
	return n * n


def div(a, b):
	try:
		d = a / b
	except ZeroDivisionError:
		return 'Cannot Divide by Zero'
	else:
		return d


def mod(a, b):
	try:
		d = a % b
	except ZeroDivisionError:
		return 'Cannot Divide by Zero'
	else:
		return d


def primenumber(n):
	c = 0
	for i in range(1, n + 1):
		if n % i == 0:
			c += 1

		if c == 2:
			return True
		return False


def fib(n):
	a = 0
	b = 1
	c = 0
	fibonacii = []
	for i in range(n):
		fibonacii.append(a)
		c = a + b
		a = b
		b = c

	return tuple(fibonacii)

