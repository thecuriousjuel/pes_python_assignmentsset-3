"""
print the last line as first line and first line as last line 
(Reverse the lines of the file)

"""

with open('file1.txt', 'r') as rf:
    content = rf.readlines()
    for i in content[::-1]:
        print(i, end='')

print('--' * 35)

"""
print characters of file from last character of file till 
the first character of the file.(Reverse entire contents of file)

"""

with open('file1.txt', 'r') as rf:
    content = rf.read()
    print(content[::-1])

