import calendar


print(calendar.calendar(2016, c=10))

print('Number of leap days between 1980 and 2025 : ', calendar.leapdays(1980, 2025))


year = int(input('Enter a valid year : '))

if calendar.isleap(year):
    print(f'{year} is a leap year')
else:
    print(f'{year} is not a leap year')


print(calendar.month(2016, 6))
