pound_to_kg_rate = 0.453592

in_pounds = float(input('Enter the weight in Pound : '))

assert in_pounds > 0, 'Negative Weight'

in_kgs = pound_to_kg_rate * in_pounds

assert in_kgs < 100, 'More than 100 Kgs'

print('Weight in Kgs : %.2f' % in_kgs)
