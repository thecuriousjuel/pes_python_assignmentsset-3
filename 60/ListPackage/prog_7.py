import random

int_list = []

for i in range(10):
    int_list.append(random.randint(1,10))

print('Integer list : ', int_list)

part_1 = int_list[:len(int_list) // 2]
part_2 = int_list[len(int_list) // 2:]

print('1st slice : ', part_1)
print('2nd slice : ', part_2)

print('Repeating 5 times : ', int_list * 5)

new_int_list = [random.randint(1, 10) for i in range(10)]
print('New Integer List : ', new_int_list)

print('Concatinating Integer List and New Integer List : ', int_list+new_int_list)


