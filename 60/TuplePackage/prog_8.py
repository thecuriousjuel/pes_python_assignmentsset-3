import random

int_list = []

for i in range(10):
    int_list.append(random.randint(1,10)) # could not find tutorial example hence randomly initialized 
    
int_set = tuple(int_list)

print('Integer Tuple : ', int_set)

part_1 = int_set[:len(int_set) // 2]
part_2 = int_set[len(int_set) // 2:]

print('1st slice : ', part_1)
print('2nd slice : ', part_2)

print('Repeating 5 times : ', int_set * 5)

new_int_set = tuple([random.randint(1, 10) for i in range(10)])
print('New Integer Tuple : ', new_int_set)

print('Concatinating Integer Tuple and New Integer Tuple : ', int_set+new_int_set)


