"""
No cmp() in python3.6!!! I used python2.7 to run this script

"""


dict1 = {
    'ID001' : 'Sam',
    'ID002' : 'Arnold',
    'ID003' : 'Polard'
}

dict2 = {
    'ID004' : 'James',
    'ID005' : 'Thomas',
    'ID006' : 'Percy'
}

dict3 = {
    'ID007' : 'Ellis',
    'ID008' : 'Nikhil',
    'ID009' : 'Jonathan'
}

print 'Dictionary 1 : ', dict1
print 'Dictionary 2 : ', dict2
print 'Dictionary 3 : ', dict3

if cmp(dict1, dict2) > 0 and cmp(dict1, dict3):
    print('Largest is Dictionary 1')
elif cmp(dict2, dict3) > 0:
    print('Largest is Dictionary 2')
else:
    print('Largest is Dictionary 3')

dict1['ID010'] = 'Moore'
dict2['ID017'] = 'Lana'

print 'Length of Dictionary 1 : ', len(dict1)
print 'Length of Dictionary 2 : ', len(dict2)
print 'Length of Dictionary 3 : ', len(dict3)

dict_to_string = str(dict1) + str(dict2) + str(dict3)

print(dict_to_string)
