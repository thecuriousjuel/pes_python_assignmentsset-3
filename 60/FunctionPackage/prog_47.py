def extend_it(mylist = [], mytuple = ()):
    mytuple = list(mytuple)
    mytuple.extend(mylist)
    return tuple(mytuple)


t, l = (1, 2, 3), [5, 6, 7]

print('Sample Tuple : ', t)
print('Sample List : ', l)

print('Extended Tuple : ', extend_it(l, t))
