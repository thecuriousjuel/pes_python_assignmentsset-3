def palindrome(string = None):
    if string is None:
        return 'Invalid String!'
    new_string = string[::-1]
    
    if string == new_string:
        return 'Palindrome'
    else:
        return 'Not Palindrome' 
        

get_string = input('Enter a String : ')

print(palindrome(get_string))
print(palindrome())
