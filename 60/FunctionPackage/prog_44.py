import random

add = lambda a, b : a + b
sub = lambda a, b : a - b
mul = lambda a, b : a * b
div = lambda a, b : a / b
mod = lambda a, b : a % b
floor_div = lambda a, b : a // b

num1 = random.randint(1, 10)
num2 = random.randint(1, 10)


print(f'Addition : {num1} + {num2} = ', add(num1, num2))
print(f'Subtraction : {num1} - {num2} = ', sub(num1, num2))
print(f'Multiplication : {num1} x {num2} = ', mul(num1, num2))
print(f'Division : {num1} / {num2} = ', div(num1, num2))
print(f'Modulus : {num1} % {num2} =  ', mod(num1, num2))
print(f'Floor Division : {num1} // {num2} = ', floor_div(num1, num2))

