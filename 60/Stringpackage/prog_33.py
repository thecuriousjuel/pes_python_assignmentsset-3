list1 = ['Mumbai', 'Delhi', 'Bangalore', 'Hyderabad', 'Ahmedabad']

print('Cities : ', list1)

list1.append('Kolkata')
print('New City Added : ', list1)

list1.insert(3, 'Pune')
print('New City Added at 4th Position: ', list1)

list1.sort()
print('Sorted List : ', list1)

list1.sort(reverse=True)
print('Sorted List in reverse : ', list1)

list1.pop()
list1.pop()
list1.pop()

print('Last three elements deleted : ', list1)
