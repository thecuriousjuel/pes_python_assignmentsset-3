import random

integer_list = [random.randint(1, 100) for i in range(10)]

print('Generated Integer List : ', integer_list)
n = int(input('Enter the number to search : '))

integer_list.sort()

s = 0
l = len(integer_list) - 1 

flag = 0


while s <= l:
    mid = (s + l) // 2
    
    if n < integer_list[mid]:
        l = mid - 1
    elif n > integer_list[mid]:
        s = mid + 1
    else:
        flag = 1
        break

if flag == 1:
    print('Success!')
else:
    print('Unsuccessful search!')