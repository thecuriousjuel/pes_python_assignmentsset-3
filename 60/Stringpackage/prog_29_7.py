string = 'lorem IPSUM olor SIT amet'

print('Partitioning the string : ', string.partition(' '))
print('Transition Table : ', string.maketrans('123', 'abc'))
print('Right Partitioning the string: ', string.rpartition(' '))
print('Replace SIT with sit : ', string.replace('SIT', 'sit'))
print('Splitting the string : ', string.split(' '))
